package logger

import (
	"fmt"
	"io"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/constants"
)

func FLogInfof(writer io.Writer, prefix string, format string, a ...interface{}) {
	fmt.Fprintf(writer, "[%-20s | %-5s [%s] %-15s", prefix, getMode(), getActualTime(), fmt.Sprintf(format, a...))
}

func FLogInfo(writer io.Writer, prefix string, a ...interface{}) {
	fmt.Fprintf(writer, "[%-20s | %-5s [%s] %-15s\n", prefix, getMode(), getActualTime(), fmt.Sprint(a...))
}

func SLogInfof(prefix string, format string, a ...interface{}) string {
	return fmt.Sprintf("[%-20s | %-5s [%s] %-15s", prefix, getMode(), getActualTime(), fmt.Sprintf(format, a...))
}

func getActualTime() string {
	return time.Now().Format(constants.CoreTimeFormat)
}

func getMode() string {
	if !gin.IsDebugging() {
		return "Release]"
	} else {
		return "Debug]"
	}
}
