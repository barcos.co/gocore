package controllers

import (
	"gitlab.com/barcos.co/gocore/models"
)

type BaseController struct {
	apiRoot string
	models.NamedInstance
}

func NewController() *BaseController {
	return &BaseController{}
}

func (controller *BaseController) SetApiRoot(apiRoot string) {
	controller.apiRoot = apiRoot
}

func (controller *BaseController) GetApiRoot() string {
	return controller.apiRoot
}

type BaseAuthController struct {
	profilePath         string
	tokenPath           string
	registrationPath    string
	validationTokenPath string
	BaseController
}

func (controller *BaseAuthController) SetProfilePath(profilePath string) {
	controller.profilePath = profilePath
}

func (controller *BaseAuthController) GetProfilePath() string {
	return controller.profilePath
}

func (controller *BaseAuthController) SetTokenPath(tokenPath string) {
	controller.tokenPath = tokenPath
}

func (controller *BaseAuthController) GetTokenPath() string {
	return controller.tokenPath
}

func (controller *BaseAuthController) SetRegistrationPath(registrationPath string) {
	controller.registrationPath = registrationPath
}

func (controller *BaseAuthController) GetRegistrationPath() string {
	return controller.registrationPath
}

func (controller *BaseAuthController) SetValidationTokenPath(validationTokenPath string) {
	controller.validationTokenPath = validationTokenPath
}

func (controller *BaseAuthController) GetValidationTokenPath() string {
	return controller.validationTokenPath
}
