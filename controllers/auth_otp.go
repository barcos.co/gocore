package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/factory"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"
)

type OTPAuthController struct {
	authRepository       interfaces.IOTPAuthRepository
	authService          interfaces.IOTPAuthService
	authMessagingService interfaces.IOTPAuthMessagingService

	BaseAuthController
}

// NewOTPAuthController
func NewOTPAuthController(
	authRepository interfaces.IOTPAuthRepository,
	authService interfaces.IOTPAuthService,
	authMessagingService interfaces.IOTPAuthMessagingService) *OTPAuthController {

	controller := OTPAuthController{
		authRepository:       authRepository,
		authService:          authService,
		authMessagingService: authMessagingService,
	}

	authRepository.SetOTPAuthController(&controller)
	authService.SetOTPAuthController(&controller)
	authMessagingService.SetOTPAuthController(&controller)

	return &controller
}

func (controller *OTPAuthController) HasOTPAuthRepository() bool {
	return controller.authRepository != nil
}

func (controller *OTPAuthController) GetOTPAuthRepository() interfaces.IOTPAuthRepository {
	return controller.authRepository
}

func (controller *OTPAuthController) HasOTPAuthService() bool {
	return controller.authService != nil
}

func (controller *OTPAuthController) GetOTPAuthService() interfaces.IOTPAuthService {
	return controller.authService
}

func (controller *OTPAuthController) HasOTPAuthMessagingService() bool {
	return controller.authMessagingService != nil
}
func (controller *OTPAuthController) GetOTPAuthMessagingService() interfaces.IOTPAuthMessagingService {
	return controller.authMessagingService
}

func (controller *OTPAuthController) Register(ctx *gin.Context) {
	var someErr errors.Error
	var user *models.User
	var jsonReq request.OTPAuthRegisterPayload

	if err := ctx.ShouldBindJSON(&jsonReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	// currentLocale := language.English
	// lans, _, err := language.ParseAcceptLanguage(ctx.GetHeader(constants.AcceptLanguageHeader))
	// if err == nil || len(lans) > 0 {
	// 	currentLocale = lans[0]
	// }
	user, someErr = controller.GetOTPAuthService().RegisterUserByEmail(jsonReq)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusCreated, factory.NewRegisterResponse(user))
}

func (controller *OTPAuthController) Login(ctx *gin.Context) {
	var someErr errors.Error
	var queryReq request.OTPAuthLoginPayload

	if err := ctx.ShouldBindQuery(&queryReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	someErr = controller.GetOTPAuthService().Login(queryReq.Email)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"sent": true})
}

func (controller *OTPAuthController) Token(ctx *gin.Context) {

	var someErr errors.Error
	var jsonRes *request.AuthToken
	var jsonReq *request.OTPAuthPayload

	if ctx.Request.URL.Fragment != "" {
		someErr.SetBadRequest()
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	if err := ctx.ShouldBindJSON(&jsonReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	jsonRes, someErr = controller.GetOTPAuthService().AuthenticateUser(*jsonReq)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.Header(constants.CacheControlHeader, constants.NoStore)
	ctx.Header(constants.PragmaHeader, constants.NoCache)

	ctx.JSON(http.StatusOK, jsonRes)
}

func (controller *OTPAuthController) ValidateToken(ctx *gin.Context) {
	if controller != nil && controller.GetOTPAuthService() != nil {
		controller.GetOTPAuthService().ValidateToken(ctx)
		// if token not valid the req aborts
		if !ctx.IsAborted() {
			ctx.JSON(http.StatusOK, gin.H{"token_valid": true})
		}
	}
}

func (controller *OTPAuthController) Profile(ctx *gin.Context) {
	var someErr errors.Error
	var user *models.User

	claims := helpers.GetRequestClaims(ctx)

	if !controller.HasOTPAuthService() {
		someErr.SetInternalServerError(fmt.Errorf("error: OTPAuthService not found"))
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	user, someErr = controller.GetOTPAuthService().FindProfileByEmail(claims.Email)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusOK, user)
}
