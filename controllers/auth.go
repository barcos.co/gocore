package controllers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/factory"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"
)

/*
AuthController This Auth Controller follow the logic described by the
section 4.3 Resource Owner Password Credentials Grant
of The OAuth 2.0 Authorization Framework

https://datatracker.ietf.org/doc/html/rfc6749#section-4.3
*/
type AuthController struct {
	authRepository       interfaces.IAuthRepository
	authService          interfaces.IAuthService
	authMessagingService interfaces.IAuthMessagingService

	verifyAccountPath string
	resetPasswordPath string
	BaseAuthController
}

// NewAuthController
func NewAuthController(
	authRepository interfaces.IAuthRepository,
	authService interfaces.IAuthService,
	authMessagingService interfaces.IAuthMessagingService) *AuthController {

	controller := AuthController{
		authRepository:       authRepository,
		authService:          authService,
		authMessagingService: authMessagingService,
	}

	authRepository.SetAuthController(&controller)
	authService.SetAuthController(&controller)
	authMessagingService.SetAuthController(&controller)

	return &controller
}

func (controller *AuthController) HasAuthRepository() bool {
	return controller.authRepository != nil
}

func (controller *AuthController) GetAuthRepository() interfaces.IAuthRepository {
	return controller.authRepository
}

func (controller *AuthController) HasAuthService() bool {
	return controller.authService != nil
}

func (controller *AuthController) GetAuthService() interfaces.IAuthService {
	return controller.authService
}

func (controller *AuthController) HasAuthMessagingService() bool {
	return controller.authMessagingService != nil
}
func (controller *AuthController) GetAuthMessagingService() interfaces.IAuthMessagingService {
	return controller.authMessagingService
}

func (controller *AuthController) SetVerifyAccountPath(verifyAccountPath string) {
	controller.verifyAccountPath = verifyAccountPath
}

func (controller *AuthController) GetVerifyAccountPath() string {
	return controller.verifyAccountPath
}

func (controller *AuthController) SetResetPasswordPath(resetPasswordPath string) {
	controller.resetPasswordPath = resetPasswordPath
}

func (controller *AuthController) GetResetPasswordPath() string {
	return controller.resetPasswordPath
}

func (controller *AuthController) Token(ctx *gin.Context) {

	var someErr errors.Error
	var jsonRes *request.AuthToken
	var queryReq *request.AuthTokenRequestPayload

	if ctx.Request.URL.Fragment != "" {
		someErr.SetBadRequest()
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	if err := ctx.ShouldBindQuery(&queryReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	if !helpers.HasRequestGrantType(queryReq.GrantType) {
		someErr.SetBadRequest()
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	email, password, ok := ctx.Request.BasicAuth()
	someErr.SetUnauthorized()

	if queryReq.GrantType == constants.PasswordGrantType {
		if !ok || email == "" || password == "" {
			ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
			return
		}

		jsonRes, someErr = controller.GetAuthService().AuthenticateUser(email, password)
	}

	if queryReq.GrantType == constants.RefreshTokenGrantType {
		jsonRes, someErr = controller.GetAuthService().RefreshAccessToken(queryReq.RefreshToken)
	}

	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	// TODO -> ensure httpOnly and secured cookie
	// c.SetCookie(TokenCookieName, jwtToken, TokenCookieMaxAge, "/", "", false, false)

	ctx.Header(constants.CacheControlHeader, constants.NoStore)
	ctx.Header(constants.PragmaHeader, constants.NoCache)

	ctx.JSON(http.StatusOK, jsonRes)

}

func (controller *AuthController) Register(ctx *gin.Context) {
	var someErr errors.Error
	var user *models.User
	var jsonReq request.AuthRegisterPayload

	if err := ctx.ShouldBindJSON(&jsonReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	// currentLocale := language.English
	// lans, _, err := language.ParseAcceptLanguage(ctx.GetHeader(constants.AcceptLanguageHeader))
	// if err == nil || len(lans) > 0 {
	// 	currentLocale = lans[0]
	// }
	user, someErr = controller.GetAuthService().RegisterUserByEmail(jsonReq)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusCreated, factory.NewRegisterResponse(user))
}

func (controller *AuthController) VerifyAccount(ctx *gin.Context) {
	var someErr errors.Error
	var queryReq request.AuthVerficationPayload

	if err := ctx.ShouldBindQuery(&queryReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	someErr = controller.GetAuthService().VerifyUserEmail(queryReq)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func (controller *AuthController) ResetPassword(ctx *gin.Context) {

	var someErr errors.Error
	var queryReq request.AuthResetPasswordQuery
	var jsonReq request.AuthResetPasswordJSON

	if err := ctx.ShouldBindQuery(&queryReq); err != nil {
		someErr = errors.NewBindingTypeError(err)
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	if queryReq.VerificationToken != "" {
		if err := ctx.ShouldBindJSON(&jsonReq); err != nil {
			someErr = errors.NewBindingTypeError(err)
			ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
			return
		}
		// change the user pwd
		someErr = controller.GetAuthService().ResetPassword(jsonReq)
	} else {
		// send an email for chnaging the user pwd
		someErr = controller.GetAuthService().SendResetPasswordEmail(queryReq.Email)
	}

	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func (controller *AuthController) ValidateToken(ctx *gin.Context) {
	if controller != nil && controller.GetAuthService() != nil {
		controller.GetAuthService().ValidateToken(ctx)
		// if token not valid the req aborts
		if !ctx.IsAborted() {
			ctx.JSON(http.StatusOK, gin.H{"token_valid": true})
		}
	}
}

func (controller *AuthController) Profile(ctx *gin.Context) {
	var someErr errors.Error
	var user *models.User

	claims := helpers.GetRequestClaims(ctx)

	if !controller.HasAuthService() {
		someErr.SetInternalServerError(fmt.Errorf("error: AuthService not found"))
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	user, someErr = controller.GetAuthService().FindProfileByEmail(claims.Email)
	if !someErr.IsNilError() {
		ctx.JSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	ctx.JSON(http.StatusOK, user)
}
