package helpers

import (
	gonanoid "github.com/matoous/go-nanoid/v2"
	"gitlab.com/barcos.co/gocore/errors"
	"golang.org/x/crypto/bcrypt"
)

const HashDefaultCost = bcrypt.DefaultCost
const DefaultRandomStringLength = 14

func GenerateShortRandomString(length ...int) (string, error) {
	strLegtn := DefaultRandomStringLength
	if len(length) > 0 {
		strLegtn = length[0]
	}
	code, err := gonanoid.New(strLegtn)
	if err != nil {
		return "", err
	}

	return code, nil
}

// NewHashedPasswordFromStringWithCost returns a hashed password string from a given password
// Max cost is 31
func NewHashedPasswordFromStringWithCost(password string, cost int) (string, errors.Error) {
	var someErr errors.Error
	hash, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	if err != nil {
		someErr.SetInternalServerError(err)
		return "", someErr
	}
	return string(hash), someErr.SetNilError()
}

// NewHashedPasswordFromString returns a hashed password string from a given password
func NewHashedPasswordFromString(password string) (string, errors.Error) {
	return NewHashedPasswordFromStringWithCost(password, HashDefaultCost)
}

// ComparePasswords handles password hash compare
func ComparePasswords(hashedPwd string, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(password)) == nil
}

func VerifyShortRandomStringDefaultLength(shortRandomString string) errors.Error {
	someErr := errors.Error{}

	if shortRandomString == "" || len(shortRandomString) != DefaultRandomStringLength {
		someErr.SetBadRequest()
		return someErr
	}
	someErr.SetNilError()
	return someErr
}
