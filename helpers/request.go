package helpers

import "gitlab.com/barcos.co/gocore/constants"

func HasRequestGrantType(param string) bool {
	hasGrantType := false

	switch param {
	case constants.RefreshTokenGrantType:
		hasGrantType = true
	case constants.PasswordGrantType:
		hasGrantType = true
	default:
		hasGrantType = false
	}

	return hasGrantType

}
