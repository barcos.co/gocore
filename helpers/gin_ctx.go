package helpers

import (
	"gitlab.com/barcos.co/gocore/constants"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

// GetRequestJWT gets the JWT on the actual Request
func GetRequestJWT(c *gin.Context) *jwt.Token {
	token, _ := c.Get(constants.ReqJWTKey)
	return token.(*jwt.Token)
}

// GetRequestClaims gets the JWT on the actual Request
func GetRequestClaims(c *gin.Context) *JWTClaims {
	claims, _ := c.Get(constants.ReqClaimsKey)
	return claims.(*JWTClaims)
}

// GetRequestClaims gets the JWT on the actual Request
func GetRequestAPIKey(c *gin.Context) *string {
	apiKey, _ := c.Get(constants.ReqAPIKey)
	return apiKey.(*string)
}
