package helpers

import (
	"fmt"
	"log"
	"net"

	"github.com/chidiwilliams/flatbson"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/system"
)

type AppURL struct {
	FrontendURL string
	APIURL      string
}

func NewAppURL() AppURL {
	url := AppURL{}
	protocol := "https://"
	port := ""
	portTmp := system.GetSystemConfigs().GetPort()
	domain := system.GetSystemConfigs().GetDomain()

	frontendURL := system.GetSystemConfigs().GetFrontendURL()
	isDebug := system.GetSystemConfigs().IsDebugMode()

	localIp := GetOutboundIP()

	if isDebug {
		protocol = "http://"
		if domain == "localhost" || domain == "" {
			domain = localIp.String()
		}
		port = ":" + portTmp
	}

	url.APIURL = fmt.Sprintf("%s%s%s", protocol, domain, port)
	url.FrontendURL = frontendURL
	return url
}

// Get preferred outbound ip of this machine
func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

func FlatObject(updateData interface{}) (map[string]interface{}, errors.Error) {
	var someErr errors.Error
	flat, err := flatbson.Flatten(updateData)
	if err != nil {
		someErr.SetInternalServerError()
		return nil, someErr
	}

	someErr.SetNilError()
	return flat, someErr
}
