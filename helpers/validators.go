package helpers

import (
	"reflect"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/logger"
	"gitlab.com/barcos.co/gocore/models"
)

type CoreValidator struct {
	Name          string
	ValidatorFunc validator.Func
}

var externalValidator []CoreValidator = []CoreValidator{}

func AddValidator(name string, validatorFunc validator.Func) {
	externalValidator = append(externalValidator, CoreValidator{
		Name: name, ValidatorFunc: validatorFunc,
	})
}

func RegisterCustomsValidators() {
	//gocore Default Validators
	AddValidator("gocoreGender", gocoreGender)
	AddValidator("gocorePassword", gocorePassword)

	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterTagNameFunc(customTagNameFunc)
		// v.RegisterValidation("bukyMobilePhone", bukyMobilePhone)
		// v.RegisterValidation("objectID", objectID)

		// register external declared validators
		for _, eV := range externalValidator {
			v.RegisterValidation(eV.Name, eV.ValidatorFunc)
			logger.SLogInfof("Validator Service", "adding validator "+eV.Name)
		}

	}
}

func customTagNameFunc(fld reflect.StructField) string {
	return fld.Tag.Get("json")
}

// var objectID validator.Func = func(fl validator.FieldLevel) bool {
//     isValid := false
//     if someobjectID, ok := fl.Field().Interface().(primitive.ObjectID); ok {
//         _, someErr := helpers.VerifyObjectIDs(someobjectID.Hex())
//         isValid = someErr.IsNilError()
//     }
//     return isValid
// }

var gocoreGender validator.Func = func(fl validator.FieldLevel) bool {
	isValid := false
	if gender, ok := fl.Field().Interface().(constants.UserGender); ok {
		var anyUser models.User
		if err := anyUser.SetGender(gender); err == nil {
			isValid = true
		}
	}

	return isValid
}

var gocorePassword validator.Func = func(fl validator.FieldLevel) bool {
	isValid := false
	if password, ok := fl.Field().Interface().(string); ok {
		if len(password) > 7 {
			isValid = true
		}
	}

	return isValid
}
