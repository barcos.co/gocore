package helpers

import (
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/system"
)

type SigningMethodType = *jwt.SigningMethodHMAC

// JWTClaims Claims defines jwt claims
type JWTClaims struct {
	jwt.RegisteredClaims `bson:",inline"`
	Email                string               `json:"email,omitempty"`
	FirstName            string               `json:"first_name,omitempty"`
	LastName             string               `json:"last_name,omitempty"`
	Gender               constants.UserGender `json:"gender,omitempty"`
	DocumentNumber       string               `json:"document_number,omitempty"`
	UserGroups           []string             `json:"user_groups,omitempty"`
	Scopes               []string             `json:"scopes,omitempty"`
}

const RefreshTokenDuration = 7 * 24 * time.Hour
const AccessTokenDefaultDuration = 5 * time.Minute
const AccessTokenDefaultDurationDEBUGMode = 5 * time.Hour
const NonAuthTokenDefaultDuration = 48 * time.Hour
const DefaultRefreshTokenRemainingTime = 24 * time.Hour
const one_second = 1 * time.Second

var AuthJWTSecretBytes = []byte(os.Getenv(constants.AUTH_JWT_SECRET_ENV))
var SigningMethod SigningMethodType = jwt.SigningMethodHS256

// NewAccessJWT handles generation of a jwt code
// duration default -> 5 Minutes
func NewAccessJWT(claims JWTClaims) (string, float64, error) {
	token := jwt.NewWithClaims(SigningMethod, claims)
	tokenString, err := token.SignedString(AuthJWTSecretBytes)
	return tokenString, claims.ExpiresAt.Sub(claims.IssuedAt.Time).Seconds(), err
}

// NewRefreshJWT generates a refresh token
func NewRefreshJWT(userID string, email string) (string, error) {
	claims := &JWTClaims{
		RegisteredClaims: GetRegisteredClaims(userID, RefreshTokenDuration),
		Email:            email,
	}

	token := jwt.NewWithClaims(SigningMethod, claims)
	tokenString, err := token.SignedString(AuthJWTSecretBytes)
	return tokenString, err
}

// NewNonAuthJWT
func NewNonAuthJWT(email string) (string, error) {
	return NewNonAuthJWTWithExpirationTime(email, NonAuthTokenDefaultDuration)
}

func NewNonAuthJWTWithExpirationTime(email string, expiration time.Duration) (string, error) {
	// Define token expiration time
	now := time.Now().Add(one_second)
	expirationTime := now.Add(expiration)
	// expirationTime := time.Now().Add(5 * time.Second)

	// Define the payload and exp time
	claims := &JWTClaims{
		Email: email,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
			IssuedAt:  jwt.NewNumericDate(now),
		},
	}

	// Generate token
	token := jwt.NewWithClaims(SigningMethod, claims)

	// Sign token with secret key encoding
	tokenString, err := token.SignedString(AuthJWTSecretBytes)
	return tokenString, err
}

// VerifyJWT verifies a JWT
func VerifyJWT(tokenString string, skipClaimsValidation bool) (bool, *jwt.Token, *JWTClaims, error) {
	claims := &JWTClaims{}
	parser := jwt.Parser{SkipClaimsValidation: skipClaimsValidation, ValidMethods: []string{SigningMethod.Name}}
	tkn, err := parser.ParseWithClaims(tokenString, claims, customKeyFunc)
	if err != nil {
		return false, tkn, claims, err
	}
	return tkn.Valid, tkn, claims, nil
}

// DecodeJWT handles decoding a jwt token
func DecodeJWT(tokenString string, skipClaimsValidation bool) (*jwt.Token, *JWTClaims, error) {
	_, tkn, claims, err := VerifyJWT(tokenString, skipClaimsValidation)
	if err != nil || !tkn.Valid {
		return tkn, claims, err
	}
	return tkn, claims, nil
}

func customKeyFunc(token *jwt.Token) (interface{}, error) {
	if _, ok := token.Method.(SigningMethodType); !ok {
		return nil, jwt.ErrSignatureInvalid
	}
	return AuthJWTSecretBytes, nil
}

func GetRegisteredClaims(userID string, duration ...time.Duration) jwt.RegisteredClaims {
	jwtDuration := AccessTokenDefaultDuration
	now := time.Now().Add(one_second)

	issuer := system.GetSystemConfigs().GetAppName()
	if issuer == "" {
		issuer = "barcos.co"
	}

	if gin.IsDebugging() {
		jwtDuration = AccessTokenDefaultDurationDEBUGMode
	}

	if len(duration) > 0 {
		jwtDuration = duration[0]
	}

	return jwt.RegisteredClaims{
		Subject:   userID,
		Issuer:    issuer,
		IssuedAt:  jwt.NewNumericDate(now),
		ExpiresAt: jwt.NewNumericDate(now.Add(jwtDuration)),
	}
}
