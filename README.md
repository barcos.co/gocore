# Go Core - OAuth2 🛡️🔐 Core Server

Add `OAuth2` functionality to your project with this pkg written in Golang<br/>
It just implements the 4.3 Resource Owner Password Credentials Grant<br/>
https://datatracker.ietf.org/doc/html/rfc6749#section-4.3

The project makes use of following dependencies

1. https://github.com/gin-gonic/gin
2. https://github.com/go-playground/validator/v10
3. https://github.com/google/uuid
4. https://github.com/golang-jwt/jwt/v4
5. https://github.com/chidiwilliams/flatbson
6. https://github.com/matoous/go-nanoid/v2
7. https://golang.org/x/crypto
8. https://golang.org/x/text
9. All other indirect deps

## Example - How to implement the core pkg ?

You could see an example how to add the code to your Golang project - Please visit the example project https://gitlab.com/barcos.co/gocore-example

## Installation

`go get gitlab.com/barcos.co/gocore`

## Tagging a Version

1. list all tags -> `git tag -n`
2. delete tag `git tag -d tag_name`
3. `git tag vx.x.x` example `export V=v0.0.79 && git tag $V && git push origin $V`

- https://blog.golang.org/publishing-go-modules^
- https://blog.golang.org/v2-go-modules

## Authors and acknowledgment

Just me - Helmer Barcos. 2022
https://barcos.co

## License

MIT
