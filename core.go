package gocore

import (
	"github.com/go-playground/validator/v10"
	"gitlab.com/barcos.co/gocore/controllers"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/http"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/services"
	"gitlab.com/barcos.co/gocore/system"
)

func LoadSystemConfigs() {
	GetSystemConfigs()
}

func GetSystemConfigs() *system.SystemConfig {
	return system.GetSystemConfigs()
}

func NewServer() *http.Server {
	return http.NewServer()
}

func AddValidator(name string, validator validator.Func) {
	helpers.AddValidator(name, validator)
}

func NewGetRouter(path string, handlers ...interfaces.IHanlder) *http.Router {
	return http.NewGetRouter(path, handlers...)
}

func NewPutRouter(path string, handlers ...interfaces.IHanlder) *http.Router {
	return http.NewPutRouter(path, handlers...)
}

func NewPostRouter(path string, handlers ...interfaces.IHanlder) *http.Router {
	return http.NewPostRouter(path, handlers...)
}

func NewDeleteRouter(path string, handlers ...interfaces.IHanlder) *http.Router {
	return http.NewDeleteRouter(path, handlers...)
}

func NewAuthController(
	authRepository interfaces.IAuthRepository,
	authService interfaces.IAuthService,
	authMessagingService interfaces.IAuthMessagingService) *controllers.AuthController {
	return controllers.NewAuthController(authRepository, authService, authMessagingService)
}

func NewOTPAuthController(
	authRepository interfaces.IOTPAuthRepository,
	authService interfaces.IOTPAuthService,
	authMessagingService interfaces.IOTPAuthMessagingService) *controllers.OTPAuthController {
	return controllers.NewOTPAuthController(authRepository, authService, authMessagingService)
}

func NewAuthService() *services.AuthService {
	return services.NewAuthService()
}

func NewOTPAuthService() *services.OTPAuthService {
	return services.NewOTPAuthService()
}
