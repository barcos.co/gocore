package errors

import (
	"fmt"
	"net/http"

	"github.com/go-playground/validator/v10"
)

const (
	BindingErr      = Code("BINDING_ERR")
	BadRequestErr   = Code("BAD_REQUEST")
	UnAuthorizedErr = Code("UNAUTHORIZED")
	ForbiddenErr    = Code("FORBIDDEN")
	NotFoundErr     = Code("NOT_FOUND")
	InternalErr     = Code("INTERNAL")
)

// NewBindingTypeError geneartes an error for the gin Ctx
func NewBindingTypeError(err error) Error {
	var someError Error
	someError.ErrorCode = BindingErr
	someError.HttpErrorCode = http.StatusBadRequest
	someError.PrimitiveErr = &err

	if _, ok := err.(validator.ValidationErrors); ok {
		someError.PrimitiveErr = nil
		someError.ValidationErrors = JSONErrorsFromValidationErrors(err)
	}

	return someError
}

// SetBadRequest function for 400 HTTP error code
func (e *Error) SetBadRequest(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("bad request")
	}
	e.ErrorCode = BadRequestErr
	e.HttpErrorCode = http.StatusBadRequest
	e.PrimitiveErr = &err[0]
}

// StatusUnauthorized function for 401 HTTP error code
func (e *Error) SetUnauthorized(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("content unauthorized")
	}
	e.ErrorCode = UnAuthorizedErr
	e.HttpErrorCode = http.StatusUnauthorized
	e.PrimitiveErr = &err[0]
}

// SetForbidden function for 403 HTTP error code
func (e *Error) SetForbidden(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("content forbidden")
	}
	e.ErrorCode = ForbiddenErr
	e.HttpErrorCode = http.StatusForbidden
	e.PrimitiveErr = &err[0]
}

// SetNotFound function for 404 HTTP error code
func (e *Error) SetNotFound(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("content not found")
	}
	e.ErrorCode = NotFoundErr
	e.HttpErrorCode = http.StatusNotFound
	e.PrimitiveErr = &err[0]
}

// SetInternalServerError function for 500 HTTP error code
func (e *Error) SetInternalServerError(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("internal server error. Try again or later")
	}
	e.ErrorCode = InternalErr
	e.HttpErrorCode = http.StatusInternalServerError
	e.PrimitiveErr = &err[0]
}
