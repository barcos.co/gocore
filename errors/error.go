package errors

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

// Code is the code for the errors
type Code string

const NullErr = Code("NULL")

// Error struct for whole api
type Error struct {
	ErrorCode        Code                     `bson:"error_code,omitempty" json:"error_code,omitempty"`
	HttpErrorCode    int                      `bson:"http_error_code,omitempty" json:"http_error_code,omitempty"`
	PrimitiveErr     *error                   `bson:"primitive_error_message,omitempty" json:"primitive_error_message,omitempty"`
	ValidationErrors []map[string]interface{} `bson:"validation_errors,omitempty" json:"validation_errors,omitempty"`
}

// NewError geneartes an error for the gin Ctx
func NewError(err error) Error {
	var someError Error
	someError.SetNilError()
	if err != nil {
		someError.PrimitiveErr = &err
	}
	return someError
}

// Message function on PrimitiveErr of Error
func (e *Error) Message() string {
	message := ""
	if e.PrimitiveErr != nil && *e.PrimitiveErr != nil {
		message = fmt.Sprintf("%v", (*e.PrimitiveErr).Error())
	}
	return message
}

// JSON function for converting a Error to a JSON representation
func (e *Error) JSON() map[string]interface{} {
	err := make(map[string]interface{})
	someErr := make(map[string]interface{})
	someErr["primitive_error_message"] = e.Message()
	someErr["http_error_code"] = e.HttpErrorCode
	someErr["error_code"] = e.ErrorCode
	if len(e.ValidationErrors) > 0 {
		someErr["validation_errors"] = e.ValidationErrors
	}
	err["error"] = someErr
	return err
}

// SetNilError function for nil error on Error
func (e *Error) SetNilError() Error {
	e.ErrorCode = NullErr
	e.HttpErrorCode = 0
	e.PrimitiveErr = nil
	return *e
}

// IsNilError function to comprove nil error on Error
func (e *Error) IsNilError() bool {
	if e != nil {
		return (e.HttpErrorCode == 0 && e.PrimitiveErr == nil)
	}
	return true
}

func JSONErrorsFromValidationErrors(err error) []map[string]interface{} {
	if err != nil {
		if valErrors, ok := err.(validator.ValidationErrors); ok {
			errorsArr := make([]map[string]interface{}, len(valErrors))
			for index, fieldErr := range valErrors {
				parsedErr := make(map[string]interface{})
				parsedErr["field"] = fieldErr.Field()
				parsedErr["tag"] = fieldErr.Tag()
				parsedErr["type"] = fieldErr.Type().Name()
				parsedErr["value"] = fieldErr.Value()
				errorsArr[index] = parsedErr
				// err.Translate() see example
				// https://github.com/go-playground/validator/blob/master/_examples/translations/main.go

				// parsedErr["namespace"] = fieldErr.Namespace()
				// parsedErr["struct_namespace"] = fieldErr.StructNamespace()
				// parsedErr["structfield"] = fieldErr.StructField()
				// parsedErr["actual_tag"] = fieldErr.ActualTag()
				// parsedErr["kind"] = fieldErr.Kind().String()
				// parsedErr["param"] = fieldErr.Param()
			}
			return errorsArr
		}
	}
	return nil
}

func IsEmptyErrArr(err ...error) bool {
	return err == nil || len(err) <= 0 || err[0] == nil
}
