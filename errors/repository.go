package errors

import (
	"fmt"
	"net/http"
)

const (
	DocumentDontExistErr    = Code("DOCUMENT_DONT_EXIST")
	DocumentAlreadyExistErr = Code("DOCUMENT_ALREADY_EXIST")
)

// SetDocumentWithIDDontExistError function for 404 HTTP error code
func (e *Error) SetDocumentWithIDDontExistError(filter string, err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("document with _id %v dont exist", filter)
	}
	e.ErrorCode = DocumentDontExistErr
	e.HttpErrorCode = http.StatusNotFound
	e.PrimitiveErr = &err[0]
}

// SetDocumentDontExistError function for 404 HTTP error code
func (e *Error) SetDocumentDontExistError(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("document dont exist")
	}
	e.ErrorCode = DocumentDontExistErr
	e.HttpErrorCode = http.StatusNotFound
	e.PrimitiveErr = &err[0]
}

// SetDocumentAlreadyExistError function for 400 HTTP error code
func (e *Error) SetDocumentAlreadyExistError(err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("document already exist")
	}
	e.ErrorCode = DocumentAlreadyExistErr
	e.HttpErrorCode = http.StatusBadRequest
	e.PrimitiveErr = &err[0]
}
