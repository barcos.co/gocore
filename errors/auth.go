package errors

import (
	"fmt"
	"net/http"
)

const (
	// AuthNeededErr            = errors.Code("AUTH_NEEDED")
	JWTExpiredErr            = Code("JWT_EXPIRED")
	JWTInvalidErr            = Code("JWT_INVALID")
	UserDontExistErr         = Code("USER_DONT_EXIST")
	RegisteredEmailErr       = Code("REGISTERED_EMAIL")
	EmailValidationNeededErr = Code("EMAIL_VALIDATION_NEEDED")
	NoAuthRepoErr            = Code("NO_AUTH_REPO")
)

// SetUserDontExistError function for 404 HTTP error code
func (e *Error) SetUserDontExistError(filter string, err ...error) {

	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("user %v dont exist", filter)
	}
	e.ErrorCode = UserDontExistErr
	e.HttpErrorCode = http.StatusNotFound
	e.PrimitiveErr = &err[0]
}

// SetUserAlreadyExistError function for 404 HTTP error code
func (e *Error) SetUserAlreadyExistError(filter string, err ...error) {
	if IsEmptyErrArr(err...) {
		err = make([]error, 1)
		err[0] = fmt.Errorf("user %v already exist", filter)
	}
	e.ErrorCode = RegisteredEmailErr
	e.HttpErrorCode = http.StatusForbidden
	e.PrimitiveErr = &err[0]
}

// SetNoAuthRepoError function for 400 HTTP error code
func (e *Error) SetNoAuthRepoError() {
	e.SetBadRequest()
	e.ErrorCode = NoAuthRepoErr
}

// SetEmailValidationNeededError function for 403 HTTP error code
func (e *Error) SetEmailValidationNeededError() {
	e.SetForbidden()
	e.ErrorCode = EmailValidationNeededErr
}

// SetEmailValidationNeededError function for 401 HTTP error code
func (e *Error) SetJWTExpiredError() {
	e.SetUnauthorized()
	e.ErrorCode = JWTExpiredErr
}

// SetEmailValidationNeededError function for 401 HTTP error code
func (e *Error) SetJWTInvalid() {
	e.SetForbidden()
	e.ErrorCode = JWTInvalidErr
}
