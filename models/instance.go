package models

type NamedInstance struct {
	name string
}

func (s *NamedInstance) SetName(name string) {
	s.name = name
}

func (s *NamedInstance) GetName() string {
	return s.name
}
