package models

import "gitlab.com/barcos.co/gocore/models/base"

type Company struct {
	Name string `json:"name"`
	base.Document
}
