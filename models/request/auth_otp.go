package request

import (
	"gitlab.com/barcos.co/gocore/constants"
)

type OTPAuthRegisterPayload struct {
	// TODO find a locale for verification
	// Locale    language.Tag `json:"locale" binding:"??"`
	Email          string               `json:"email" binding:"required,email"`
	FirstName      string               `json:"first_name" binding:"required,min=2,max=100"`
	LastName       string               `json:"last_name" binding:"required,min=2,max=100"`
	Gender         constants.UserGender `json:"gender" binding:"required,gocoreGender"`
	DocumentNumber string               `json:"document_number,omitempty" binding:"required,min=2,max=100"`
	// UserGroups []string               `json:"document_number,omitempty" binding:"required,min=2,max=100"`
}

type OTPAuthLoginPayload struct {
	Email string `json:"email" form:"email" binding:"required,email"`
}

type OTPAuthLoginMessagingPayload struct {
	FirstName string
	LastName  string
	Email     string
	OTPLink   string
	Gender    constants.UserGender
}

type OTPAuthPayload struct {
	OTPToken string `json:"otp_token" form:"otp_token" binding:"required"`
}
