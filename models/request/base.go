package request

import (
	"gitlab.com/barcos.co/gocore/constants"
	"golang.org/x/text/language"
)

type BaseMessagingPayload struct {
	FirstName        string
	LastName         string
	Email            string
	Locale           language.Tag
	VerificationLink string
	Gender           constants.UserGender
}
