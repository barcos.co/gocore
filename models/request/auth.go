package request

type AuthRegisterPayload struct {
	// TODO find a locale for verification
	// Locale    language.Tag `json:"locale" binding:"??"`
	Email     string `json:"email" binding:"required,email"`
	FirstName string `json:"first_name" binding:"required,min=2,max=100"`
	LastName  string `json:"last_name" binding:"required,min=2,max=100"`
	// TODO register the custom bindig
	// Gender   constants.UserGender `json:"gender" binding:"required,gocoreGender"`
	Password string `json:"password" binding:"required,min=8,gocorePassword"`
}

type AuthRegisterResult struct {
	UserId string `json:"user_id"`
	// VerificationToken string `json:"verification_token"`
}

type AuthVerficationPayload struct {
	VerificationToken string `json:"verification_token" form:"verification_token" binding:"required"`
}

type AuthRegisterMessagingPayload = BaseMessagingPayload
type AuthResetPasswordEmailMessagingPayload = BaseMessagingPayload

type AuthTokenRequestPayload struct {
	GrantType    string `json:"grant_type" form:"grant_type" binding:"required"`
	RefreshToken string `json:"refresh_token" form:"refresh_token"`
}

type AuthToken struct {
	AccessToken  string  `json:"access_token" binding:"required"`
	ExpiresIn    float64 `json:"expires_in" binding:"required"`
	RefreshToken string  `json:"refresh_token" binding:"required"`
	TokenType    string  `json:"token_type" binding:"required"`
}

type AuthResetPasswordQuery struct {
	VerificationToken string `form:"verification_token"`
	Email             string `form:"email" binding:"required,email"`
}

type AuthResetPasswordJSON struct {
	OldPassword       string `json:"old_password" binding:"required"`
	NewPassword       string `json:"new_password" binding:"required,gocorePassword"`
	VerificationToken string `json:"verification_token" binding:"required"`
}
