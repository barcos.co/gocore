package models

import (
	"fmt"
	"time"

	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/models/base"
)

type UserGroupsValidator struct {
	userGroups []string
}

func NewUserGroupsValidator(userGroups []string) *UserGroupsValidator {
	return &UserGroupsValidator{userGroups: userGroups}
}

func (validator *UserGroupsValidator) HasUserGroup(group string) bool {
	for _, value := range validator.userGroups {
		if value == group {
			return true
		}
	}
	return false
}

type User struct {
	base.Document       `bson:",inline"`
	Email               base.Email           `bson:"email,omitempty" json:"email"`
	FirstName           string               `bson:"first_name,omitempty" json:"first_name,omitempty"`
	LastName            string               `bson:"last_name,omitempty" json:"last_name,omitempty"`
	Password            string               `bson:"password,omitempty" json:"-"`
	PasswordLastResetAt *time.Time           `bson:"password_reset_at,omitempty" json:"password_reset_at,omitempty"`
	State               constants.UserState  `bson:"state,omitempty" json:"state,omitempty"`
	Gender              constants.UserGender `bson:"gender,omitempty" json:"gender,omitempty"`
	DocumentNumber      string               `bson:"document_number,omitempty" json:"document_number,omitempty"`
	UserGroups          []string             `bson:"user_groups,omitempty" json:"user_groups,omitempty"`
}

func (user *User) GetID() string {
	return user.ID
}

func (user *User) SetID(Id string) *User {
	user.ID = Id
	return user
}

func (user *User) GetEmail() string {
	return user.Email.GetEmail()
}

func (user *User) IsVerified() bool {
	return user.Email.Verified
}

func (user *User) SetVerified() *User {
	now := time.Now()
	user.Email = base.Email{}
	user.Email.Verified = true
	user.Email.VerifiedAt = &now
	user.State = constants.Active
	return user
}

func (user *User) SetUpdated(by string) *User {
	now := time.Now()
	user.UpdatedAt = &now
	user.UpdatedBy = by
	return user
}

func (u *User) SetGender(gender constants.UserGender) error {
	switch gender {
	case constants.Male:
	case constants.Female:
	case constants.Other:
	default:
		return fmt.Errorf("%v is not a valid gender", gender)
	}
	u.Gender = gender
	return nil
}
