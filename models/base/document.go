package base

import (
	"time"
)

type NanoID = string

type VerifiedDocument struct {
	VerifiedAt *time.Time `bson:"verified_at,omitempty" json:"verified_at,omitempty"`
	Verified   bool       `bson:"verified,omitempty" json:"verified"`
}

// func (d *BaseDocument) Reset() {
// 	*d = BaseDocument{}
// 	ts := time.Now()
// 	d.CreatedAt = &ts
// }

// func (d *VerifiedDocument) Reset() {
// 	*d = VerifiedDocument{}
// 	d.Verified = false
// }

func (d *VerifiedDocument) Verify() {
	ts := time.Now()
	d.VerifiedAt = &ts
	d.Verified = true
}

// type BaseDocument struct {
// 	ID        *primitive.ObjectID `bson:"_id,omitempty" json:"_id,omitempty"`
// 	CreatedAt *time.Time          `bson:"created_at,omitempty" json:"created_at,omitempty"`
// 	CreatedBy *primitive.ObjectID `bson:"created_by,omitempty" json:"created_by,omitempty"`
// 	UpdatedAt *time.Time          `bson:"updated_at,omitempty" json:"updated_at,omitempty"`
// 	UpdatedBy *primitive.ObjectID `bson:"updated_by,omitempty" json:"updated_by,omitempty"`
// }

type Document struct {
	ID        string     `bson:"_id,omitempty" json:"_id,omitempty"`
	CreatedAt *time.Time `bson:"created_at,omitempty" json:"created_at,omitempty"`
	CreatedBy string     `bson:"created_by,omitempty" json:"created_by,omitempty"`
	UpdatedAt *time.Time `bson:"updated_at,omitempty" json:"updated_at,omitempty"`
	UpdatedBy string     `bson:"updated_by,omitempty" json:"updated_by,omitempty"`
}

// func (d *BaseDocument) SetObjectID(id *primitive.ObjectID) {
// 	id
// 	d.ID = id
// }

// func (d *BaseDocument) GetObjectID() *primitive.ObjectID {
// 	return d.ID
// }

// func (d *BaseDocument) SetID(id string) error {
// 	someID, err := primitive.ObjectIDFromHex(id)
// 	if err != nil {
// 		return err
// 	}
// 	d.ID = &someID
// 	return nil
// }

// func (d *BaseDocument) GetID() string {
// 	if d.GetObjectID() != nil {
// 		return d.GetObjectID().Hex()
// 	}
// 	return ""
// }

// func (d *BaseDocument) SetCreatedBy(id string) error {
// 	someID, err := primitive.ObjectIDFromHex(id)
// 	if err != nil {
// 		return err
// 	}
// 	d.CreatedBy = &someID
// 	return nil
// }

// func (d *BaseDocument) GetCreatedBy() string {
// 	if d.GetCreatedByObjectID() != nil {
// 		return d.GetCreatedByObjectID().Hex()
// 	}
// 	return ""
// }

// func (d *BaseDocument) GetCreatedByObjectID() *primitive.ObjectID {
// 	return d.CreatedBy
// }

// func (d *BaseDocument) WasCreatedBy(creatorID string) bool {
// 	if d.GetCreatedByObjectID() != nil {
// 		return d.GetCreatedByObjectID().Hex() == creatorID
// 	}
// 	return false
// }

// func (d *BaseDocument) SetUpdatedBy(id string) error {
// 	someID, err := primitive.ObjectIDFromHex(id)
// 	if err != nil {
// 		return err
// 	}
// 	d.UpdatedBy = &someID
// 	d.updatedNow()
// 	return nil
// }

// func (d *BaseDocument) GetUpdatedBy() string {
// 	if d.GetUpdatedByObjectID() != nil {
// 		return d.GetUpdatedByObjectID().Hex()
// 	}
// 	return ""
// }

// func (d *BaseDocument) GetUpdatedByObjectID() *primitive.ObjectID {
// 	return d.UpdatedBy
// }

// func (d *BaseDocument) SelfRegistered() bool {
// 	return d.GetID() == d.GetCreatedBy()
// }

// func (d *BaseDocument) GetFlatDocument() map[string]interface{} {
// 	obj, someErr := helpers.FlatObject(d)
// 	if someErr.IsNilError() {
// 		return obj
// 	} else {
// 		return make(map[string]interface{})
// 	}
// }

// func (document *Document) updatedNow() {
// 	ts := time.Now()
// 	document.UpdatedAt = &ts
// }
