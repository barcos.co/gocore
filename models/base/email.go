package base

import (
	"fmt"
	"time"
)

// Email struct for emails
type Email struct {
	VerifiedDocument `bson:",inline"`
	Email            string `bson:"email,omitempty" json:"email"`
}

// func (e *Email) Reset() {
// 	*e = Email{}
// 	e.VerifiedDocument = VerifiedDocument{}
// 	e.VerifiedDocument.Reset()
// }

func (e *Email) SetEmail(email string) {
	if email != "" {
		e.Email = email
		e.Verified = false
		e.VerifiedAt = nil
	}
}

func (e *Email) GetEmail() string {
	return e.Email
}

func (e *Email) Verify() error {
	if e.Email == "" {
		return fmt.Errorf("email can not be verified. an email address is needed")
	}

	ts := time.Now()
	e.Verified = true
	e.VerifiedAt = &ts
	return nil
}
