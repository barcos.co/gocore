package services

import (
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/logger"
	"gitlab.com/barcos.co/gocore/models"
)

type BaseAuthService struct {
	models.NamedInstance
}

func (service *BaseAuthService) ValidateToken(ctx *gin.Context) {
	AuthenticateRequest(ctx)
}

func (service *BaseAuthService) BaseFindProfileByEmail(
	email string, repository interfaces.IBaseAuthRepository) (*models.User, errors.Error) {

	var someErr errors.Error
	var user *models.User

	if repository == nil {
		someErr.SetNoAuthRepoError()
		logger.SLogInfof(service.GetName(), "No auth repository provided")
		return nil, someErr
	}

	user, someErr = repository.FindAuthUserByEmail(email)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	return user, someErr
}

// Authenticate auth an endpoint
func AuthenticateRequest(ctx *gin.Context) {
	var someErr errors.Error
	someErr.SetUnauthorized()

	// Fetch token from the headers
	requiredToken := ctx.GetHeader(constants.AuthorizationHeader)
	if len(requiredToken) == 0 {
		ctx.AbortWithStatusJSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	splittedToken := strings.SplitN(requiredToken, " ", 2)
	if len(splittedToken) != 2 || strings.ToLower(splittedToken[0]) != "bearer" {
		someErr.SetUnauthorized(fmt.Errorf("wrong bearer token format on Authorization Header"))
		ctx.AbortWithStatusJSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	// Get email from encoded token
	jwtToken, claims, err := helpers.DecodeJWT(splittedToken[1], false)
	if err != nil {
		someErr.SetUnauthorized(err)
		ctx.AbortWithStatusJSON(someErr.HttpErrorCode, someErr.JSON())
		return
	}

	// TODO
	// if _, err := helpers.VerifyObjectIDs(claims.Subject); !err.IsNilError() {
	// 	c.AbortWithStatusJSON(someErr.HttpErrorCode, someErr.JSON())
	// 	return
	// }

	// Set the User variable so that we can easily retrieve from other middlewares
	// c.Set("User", result)
	ctx.Set(constants.ReqJWTKey, jwtToken)
	ctx.Set(constants.ReqClaimsKey, claims)

	// Call the next middlware
	ctx.Next()
}
