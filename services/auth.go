package services

import (
	"fmt"

	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/factory"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/logger"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

type AuthService struct {
	authController interfaces.IAuthController
	BaseAuthService
}

func NewAuthService() *AuthService {
	authService := AuthService{}
	authService.SetName("Auth Service")
	return &authService
}

func (service *AuthService) GetAuthController() interfaces.IAuthController {
	return service.authController
}

func (service *AuthService) SetAuthController(authController interfaces.IAuthController) {
	service.authController = authController
}

func (service *AuthService) RegisterUserByEmail(payload request.AuthRegisterPayload) (*models.User, errors.Error) {

	var userId string
	var user *models.User
	var someErr errors.Error
	controller := service.GetAuthController()

	if controller.HasAuthRepository() {

		user, someErr = factory.NewUser(payload)
		if !someErr.IsNilError() {
			return nil, someErr
		}

		userId, someErr = controller.GetAuthRepository().AddAuthUser(user)
		if !someErr.IsNilError() {
			return nil, someErr
		}

		verificationLink := service.newVerificationLink(user.GetEmail())
		mesagingPayload := *factory.NewRegisterMessagingPayload(user, verificationLink)

		if controller.HasAuthMessagingService() {
			_, someErr = controller.GetAuthMessagingService().SendVerifyEmail(mesagingPayload)
		} else {
			logger.SLogInfof(service.GetName(), "No messaging service provided")
			someErr.SetInternalServerError()
		}

		if !someErr.IsNilError() {
			controller.GetAuthRepository().DeleteAuthUserByEmail(user.GetEmail())
			return nil, someErr
		}

		user.ID = userId
		someErr.SetNilError()

	} else {
		someErr.SetNoAuthRepoError()
		logger.SLogInfof(service.GetName(), "No auth repository provided")
	}

	return user, someErr
}

func (service *AuthService) VerifyUserEmail(payload request.AuthVerficationPayload) errors.Error {
	var someErr errors.Error
	var user *models.User
	controller := service.GetAuthController()

	// is the token is not valid resend other email with another token

	_, claims, err := helpers.DecodeJWT(payload.VerificationToken, false)
	user, someErr = controller.GetAuthRepository().FindAuthUserByEmail(claims.Email)

	if !someErr.IsNilError() {
		return someErr
	}

	// user already verified
	if user.IsVerified() {
		someErr.SetForbidden()
		return someErr
	}

	if err != nil {

		v, _ := err.(*jwt.ValidationError)
		if v.Errors == jwt.ValidationErrorExpired {

			verificationLink := service.newVerificationLink(claims.Email)
			mesagingPayload := *factory.NewRegisterMessagingPayload(user, verificationLink)

			if controller.HasAuthMessagingService() {
				_, someErr = controller.GetAuthMessagingService().SendVerifyEmail(mesagingPayload)
			} else {
				logger.SLogInfof(service.GetName(), "No messaging service provided")
				someErr.SetInternalServerError()
			}

			if !someErr.IsNilError() {
				return someErr
			}

			someErr.SetJWTExpiredError()
			return someErr
		}

		if err != nil {
			someErr.SetUnauthorized(err)
		} else {
			someErr.SetBadRequest(fmt.Errorf("the %s is not valid", constants.VerificationTokenParam))
		}

		return someErr
	}

	if user.CreatedAt.Local().After(claims.IssuedAt.Local()) || claims.Email != user.GetEmail() {
		someErr.SetJWTInvalid()
		return someErr
	}

	updateData, someErr := helpers.FlatObject(factory.NewEmptyVerifiedUser())
	if !someErr.IsNilError() {
		return someErr
	}

	_, someErr = controller.GetAuthRepository().UpdateAuthUserByEmail(user.GetEmail(), updateData)
	if !someErr.IsNilError() {
		return someErr
	}

	return someErr.SetNilError()
}

func (service *AuthService) AuthenticateUser(email string, password string) (*request.AuthToken, errors.Error) {

	var err error
	var someErr errors.Error

	var expiresIn float64
	var user *models.User
	var accessToken, refreshToken string

	controller := service.GetAuthController()

	user, someErr = controller.GetAuthRepository().FindAuthUserByEmail(email)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	if !user.IsVerified() {
		someErr.SetEmailValidationNeededError()
		return nil, someErr
	}

	if !helpers.ComparePasswords(user.Password, password) {
		someErr.SetUnauthorized()
		return nil, someErr
	}

	accessToken, expiresIn, err = helpers.NewAccessJWT(factory.NewJWTClaims(user))
	if err != nil {
		someErr.SetInternalServerError(err)
		return nil, someErr
	}

	refreshToken, err = helpers.NewRefreshJWT(user.GetID(), user.GetEmail())
	if err != nil {
		someErr.SetInternalServerError(err)
		return nil, someErr
	}

	return factory.NewAuthToken(accessToken, expiresIn, refreshToken), someErr.SetNilError()
}

func (service *AuthService) RefreshAccessToken(refreshToken string) (*request.AuthToken, errors.Error) {
	var user *models.User
	var someErr errors.Error
	var authToken *request.AuthToken

	_, refreshClaims, err := helpers.DecodeJWT(refreshToken, false)
	if err != nil {
		someErr.SetUnauthorized(err)
	}

	user, someErr = service.authController.GetAuthRepository().FindAuthUserByEmail(refreshClaims.Email)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	accessToken, expiresIn, err := helpers.NewAccessJWT(factory.NewJWTClaims(user))
	if err != nil {
		someErr.SetInternalServerError(err)
		return nil, someErr
	}

	refreshToken, err = helpers.NewRefreshJWT(user.GetID(), user.GetEmail())
	if err != nil {
		someErr.SetInternalServerError(err)
		return nil, someErr
	}

	authToken = factory.NewAuthToken(accessToken, expiresIn, refreshToken)

	return authToken, someErr.SetNilError()
}

// TODO -> the user can reset a pwd by clicking a btn. He/she recive an email with a comfirmation link
// on that link he could create another password
// -> send email for restet
func (service *AuthService) SendResetPasswordEmail(email string) errors.Error {
	var user *models.User
	var someErr errors.Error
	controller := service.GetAuthController()

	if controller.HasAuthRepository() {

		if controller.HasAuthMessagingService() {

			user, someErr = controller.GetAuthRepository().FindAuthUserByEmail(email)
			if !someErr.IsNilError() {
				return someErr
			}

			verificationLink := service.newResetPasswordLink(user.GetEmail())
			mesagingPayload := *factory.NewResetPasswordMessagingPayload(user, verificationLink)
			_, someErr = controller.GetAuthMessagingService().SendResetPasswordEmail(mesagingPayload)
			if !someErr.IsNilError() {
				return someErr
			}

			// TODO send email notifying the pwd change

			someErr.SetNilError()
		} else {
			logger.SLogInfof(service.GetName(), "No messaging service provided")
			someErr.SetInternalServerError()
		}

	} else {
		someErr.SetNoAuthRepoError()
		logger.SLogInfof(service.GetName(), "No auth repository provided")
	}

	return someErr
}

// verify token and so change pwd
func (service *AuthService) ResetPassword(payload request.AuthResetPasswordJSON) errors.Error {
	var someErr errors.Error
	var user *models.User
	controller := service.GetAuthController()

	// is the token is not valid resend other email with another token

	_, claims, err := helpers.DecodeJWT(payload.VerificationToken, false)
	user, someErr = controller.GetAuthRepository().FindAuthUserByEmail(claims.Email)

	if !someErr.IsNilError() {
		return someErr
	}

	if err != nil {
		v, _ := err.(*jwt.ValidationError)
		if v.Errors == jwt.ValidationErrorExpired {
			someErr.SetJWTExpiredError()
			return someErr
		}

		if err != nil {
			someErr.SetUnauthorized(err)
		} else {
			someErr.SetBadRequest(fmt.Errorf("the %s is not valid", constants.VerificationTokenParam))
		}

		return someErr
	}

	if !user.IsVerified() || !helpers.ComparePasswords(user.Password, payload.OldPassword) {
		someErr.SetUnauthorized()
		return someErr
	}

	// store new password

	updatedUser, someErr := factory.NewUserWithPassword(payload.NewPassword)
	if !someErr.IsNilError() {
		return someErr
	}

	updateData, someErr := helpers.FlatObject(updatedUser)
	if !someErr.IsNilError() {
		return someErr
	}

	_, someErr = controller.GetAuthRepository().UpdateAuthUserByEmail(user.GetEmail(), updateData)
	if !someErr.IsNilError() {
		return someErr
	}

	// TODO send email notifying the pwd change

	return someErr.SetNilError()
}

func (service *AuthService) FindProfileByEmail(email string) (*models.User, errors.Error) {
	return service.BaseFindProfileByEmail(email, service.GetAuthController().GetAuthRepository())
}

// ValidateAPIKey func
func (s *AuthService) ValidateAPIKey(knowAPIKeys []string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var someErr errors.Error

		someErr.SetForbidden(fmt.Errorf("unauthorized api_key"))
		APIKey := c.GetHeader(constants.APIKeyHeader)

		if APIKey == "" {
			c.AbortWithStatusJSON(someErr.HttpErrorCode, someErr.JSON())
			return
		}

		if isKnowAPIKey(knowAPIKeys, APIKey) {
			// ok known APIKey
			c.Set(constants.ReqAPIKey, APIKey)
			c.Next()
			return
		}

		// TODO -> add apikey validation for web clients or other clients
		// if APIKeyValid := router.CoreService.ValidateAPIKey(json.ApiKey); !APIKeyValid {
		// 	err.PrimitiveErr = fmt.Errorf("unauthorized api_key")
		// 	c.JSON(err.ErrorCode, err.JSON())
		// 	return
		// }

		c.AbortWithStatusJSON(someErr.HttpErrorCode, someErr.JSON())
	}
}

func isKnowAPIKey(knowKeys []string, apiKey string) bool {
	for _, key := range knowKeys {
		if key == apiKey {
			return true
		}
	}
	return false
}

func (service *AuthService) newVerificationLink(email string) string {
	controller := service.GetAuthController()
	jwt, _ := helpers.NewNonAuthJWT(email)

	verifyPath := controller.GetVerifyAccountPath()
	baseURL := helpers.NewAppURL().FrontendURL + controller.GetApiRoot()
	verificationLink := fmt.Sprintf("%s%s?%s=%s", baseURL, verifyPath, constants.VerificationTokenParam, jwt)
	return verificationLink
}

func (service *AuthService) newResetPasswordLink(email string) string {
	controller := service.GetAuthController()
	jwt, _ := helpers.NewNonAuthJWT(email)

	path := controller.GetResetPasswordPath()
	baseURL := helpers.NewAppURL().FrontendURL + controller.GetApiRoot()
	verificationStr := constants.VerificationTokenParam + "=" + jwt
	emailStr := constants.EmailParam + "=" + email
	verificationLink := fmt.Sprintf("%s%s?%s&%s", baseURL, path, verificationStr, emailStr)
	return verificationLink
}
