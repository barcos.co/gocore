package services

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/factory"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/logger"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"
)

type OTPAuthService struct {
	authController interfaces.IOTPAuthController
	BaseAuthService
}

func NewOTPAuthService() *OTPAuthService {
	authService := OTPAuthService{}
	authService.SetName("Auth Service")
	return &authService
}

func (service *OTPAuthService) GetOTPAuthController() interfaces.IOTPAuthController {
	return service.authController
}

func (service *OTPAuthService) SetOTPAuthController(authController interfaces.IOTPAuthController) {
	service.authController = authController
}

func (service *OTPAuthService) RegisterUserByEmail(
	payload request.OTPAuthRegisterPayload) (*models.User, errors.Error) {

	var userId string
	var user *models.User
	var someErr errors.Error
	controller := service.GetOTPAuthController()

	if controller.HasOTPAuthRepository() {
		user = factory.NewOTPUser(payload)
		userId, someErr = controller.GetOTPAuthRepository().AddAuthUser(user)
		if !someErr.IsNilError() {
			return nil, someErr
		}

		user.ID = userId
		someErr.SetNilError()
	} else {
		someErr.SetNoAuthRepoError()
		logger.SLogInfof(service.GetName(), "No auth repository provided")
	}

	return user, someErr
}

func (service *OTPAuthService) Login(email string) errors.Error {

	// var err error
	var someErr errors.Error

	// var expiresIn float64
	var user *models.User
	// var accessToken, refreshToken string

	controller := service.GetOTPAuthController()

	user, someErr = controller.GetOTPAuthRepository().FindAuthUserByEmail(email)
	if !someErr.IsNilError() {
		return someErr
	}

	link := service.newOTPLoginLink(user.GetEmail(), false)
	mesagingPayload := *factory.NewOTPRegisterMessagingPayload(user, link)

	if controller.HasOTPAuthMessagingService() {
		_, someErr = controller.GetOTPAuthMessagingService().SendOTPLoginEmail(mesagingPayload)
	} else {
		logger.SLogInfof(service.GetName(), "No messaging service provided")
		someErr.SetInternalServerError()
	}

	// accessToken, expiresIn, err = helpers.NewAccessJWT(factory.NewJWTClaims(user))
	// if err != nil {
	// 	someErr.SetInternalServerError(err)
	// 	return nil, someErr
	// }

	// refreshToken, err = helpers.NewRefreshJWT(user.GetID(), user.GetEmail())
	// if err != nil {
	// 	someErr.SetInternalServerError(err)
	// 	return nil, someErr
	// }

	// return factory.NewAuthToken(accessToken, expiresIn, refreshToken), someErr.SetNilError()
	return someErr.SetNilError()
}

func (service *OTPAuthService) AuthenticateUser(payload request.OTPAuthPayload) (*request.AuthToken, errors.Error) {
	var err error
	var someErr errors.Error

	var expiresIn float64
	var user *models.User
	var accessToken, refreshToken string

	user, _, someErr = service.VerifyOTPToken(payload)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	accessToken, expiresIn, err = helpers.NewAccessJWT(factory.NewJWTClaims(user))
	if err != nil {
		someErr.SetInternalServerError(err)
		return nil, someErr
	}

	refreshToken, err = helpers.NewRefreshJWT(user.GetID(), user.GetEmail())
	if err != nil {
		someErr.SetInternalServerError(err)
		return nil, someErr
	}

	return factory.NewAuthToken(accessToken, expiresIn, refreshToken), someErr.SetNilError()

}

func (service *OTPAuthService) VerifyOTPToken(
	payload request.OTPAuthPayload) (*models.User, *helpers.JWTClaims, errors.Error) {

	var someErr errors.Error
	var user *models.User
	controller := service.GetOTPAuthController()

	_, claims, err := helpers.DecodeJWT(payload.OTPToken, false)
	user, someErr = controller.GetOTPAuthRepository().FindAuthUserByEmail(claims.Email)

	if !someErr.IsNilError() {
		return nil, nil, someErr
	}

	if err != nil {
		v, _ := err.(*jwt.ValidationError)
		if v.Errors == jwt.ValidationErrorExpired {
			someErr.SetJWTExpiredError()
			return nil, nil, someErr
		}

		if err != nil {
			someErr.SetUnauthorized(err)
		} else {
			someErr.SetBadRequest(fmt.Errorf("the %s is not valid", constants.VerificationTokenParam))
		}

		return nil, nil, someErr
	}

	if user.CreatedAt.Local().After(claims.IssuedAt.Local()) || claims.Email != user.GetEmail() {
		someErr.SetJWTInvalid()
		return nil, nil, someErr
	}

	updateData, someErr := helpers.FlatObject(factory.NewEmptyVerifiedUser())
	if !someErr.IsNilError() {
		return nil, nil, someErr
	}

	_, someErr = controller.GetOTPAuthRepository().UpdateAuthUserByEmail(user.GetEmail(), updateData)
	if !someErr.IsNilError() {
		return nil, nil, someErr
	}

	return user, claims, someErr.SetNilError()
}

func (service *OTPAuthService) FindProfileByEmail(email string) (*models.User, errors.Error) {
	return service.BaseFindProfileByEmail(email, service.GetOTPAuthController().GetOTPAuthRepository())
}

func (service *OTPAuthService) newOTPLoginLink(email string, priviliged bool) string {
	expiration := 5 * time.Minute
	controller := service.GetOTPAuthController()

	if priviliged {
		expiration = 48 * time.Hour
	}

	jwt, _ := helpers.NewNonAuthJWTWithExpirationTime(email, expiration)

	baseURL := helpers.NewAppURL().FrontendURL + controller.GetApiRoot()
	verificationLink := fmt.Sprintf("%s%s?%s=%s", baseURL, "/otp", constants.OTPTokenParam, jwt)
	return verificationLink
}
