package factory

import (
	"time"

	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/base"
	"gitlab.com/barcos.co/gocore/models/request"
)

func NewEmptyVerifiedUser() *models.User {
	user := models.User{}
	return user.SetVerified()
}

func NewUser(payload request.AuthRegisterPayload) (*models.User, errors.Error) {
	now := time.Now()
	hashedPassword, someErr := helpers.NewHashedPasswordFromString(payload.Password)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	user := models.User{
		FirstName: payload.FirstName,
		LastName:  payload.LastName,
		Password:  hashedPassword,
		State:     constants.Pending,
	}

	user.Email = base.Email{
		Email: payload.Email,
		VerifiedDocument: base.VerifiedDocument{
			Verified: false,
		},
	}

	user.CreatedAt = &now

	return &user, someErr
}

func NewOTPUser(payload request.OTPAuthRegisterPayload) *models.User {
	now := time.Now()
	user := models.User{
		FirstName:      payload.FirstName,
		LastName:       payload.LastName,
		State:          constants.Pending,
		Gender:         payload.Gender,
		DocumentNumber: payload.DocumentNumber,
		UserGroups:     []string{"pacientes"},
	}

	user.Email = base.Email{
		Email: payload.Email,
		VerifiedDocument: base.VerifiedDocument{
			Verified: false,
		},
	}

	user.CreatedAt = &now
	return &user
}

func NewUserWithPassword(password string) (*models.User, errors.Error) {
	now := time.Now()
	hashedPassword, someErr := helpers.NewHashedPasswordFromString(password)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	user := models.User{
		Password: hashedPassword,
		State:    constants.Active,
	}

	user.UpdatedAt = &now

	return &user, someErr
}
