package factory

import (
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"
	"golang.org/x/text/language"
)

func NewRegisterResponse(user *models.User) *request.AuthRegisterResult {
	return &request.AuthRegisterResult{UserId: user.ID}
}

func NewRegisterMessagingPayload(user *models.User,
	verificationLink string) *request.AuthRegisterMessagingPayload {
	return &request.AuthRegisterMessagingPayload{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email.Email,
		// TODO chnage language
		Locale:           language.English,
		VerificationLink: verificationLink,
		Gender:           user.Gender,
	}
}

func NewOTPRegisterMessagingPayload(user *models.User,
	link string) *request.OTPAuthLoginMessagingPayload {
	return &request.OTPAuthLoginMessagingPayload{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email.Email,
		// TODO chnage language
		OTPLink: link,
		Gender:  user.Gender,
	}
}

func NewResetPasswordMessagingPayload(user *models.User,
	verificationLink string) *request.AuthResetPasswordEmailMessagingPayload {
	return &request.AuthRegisterMessagingPayload{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email.Email,
		// TODO chnage language
		Locale:           language.English,
		VerificationLink: verificationLink,
		Gender:           user.Gender,
	}
}

func NewAuthToken(accessToken string, expiresIn float64,
	refreshToken string) *request.AuthToken {
	return &request.AuthToken{
		AccessToken:  accessToken,
		ExpiresIn:    expiresIn,
		RefreshToken: refreshToken,
		TokenType:    constants.BearerTokenType,
	}
}

func NewJWTClaims(user *models.User, scopes ...string) helpers.JWTClaims {
	claims := helpers.JWTClaims{
		RegisteredClaims: helpers.GetRegisteredClaims(user.GetID()),
		Email:            user.GetEmail(),
		FirstName:        user.FirstName,
		LastName:         user.LastName,
		DocumentNumber:   user.DocumentNumber,
		UserGroups:       user.UserGroups,
		Gender:           user.Gender,
	}

	claims.Scopes = append(claims.Scopes, scopes...)
	return claims
}
