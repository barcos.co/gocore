package http

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/barcos.co/gocore/interfaces"
)

type IHanlder = interfaces.IHanlder

type Router struct {
	method         string
	path           string
	handlers       []IHanlder
	AuthRequired   bool
	APIKeyRequired bool
}

type routersArray []*Router

func newRouter(method string, path string, handler ...IHanlder) *Router {
	r := Router{AuthRequired: false, APIKeyRequired: false}
	r.setMethod(method).setPath(path).addHandlers(handler...)

	if r.path == "" {
		id := uuid.New().String()
		r.setPath(fmt.Sprintf("/%s", id))
	}

	if r.method == "" {
		r.setMethod(http.MethodGet)
	}

	return &r
}

func (r *Router) setMethod(method string) *Router {
	r.method = method
	return r
}

func (r *Router) setPath(path string) *Router {
	r.path = path
	return r
}

func (r *Router) addHandlers(handlers ...gin.HandlerFunc) *Router {
	r.handlers = append(r.handlers, handlers...)
	return r
}

func (r *Router) setHandlers(handlers ...gin.HandlerFunc) *Router {
	r.handlers = handlers
	return r
}

func (r *Router) SetAuthRequired() *Router {
	r.AuthRequired = true
	return r
}

func (r *Router) SetAPIKeyRequired() *Router {
	r.APIKeyRequired = true
	return r
}

func NewGetRouter(path string, handlers ...IHanlder) *Router {
	return newRouter(http.MethodGet, path, handlers...)
}

func NewPutRouter(path string, handlers ...IHanlder) *Router {
	return newRouter(http.MethodPut, path, handlers...)
}

func NewPostRouter(path string, handlers ...IHanlder) *Router {
	return newRouter(http.MethodPost, path, handlers...)
}

func NewDeleteRouter(path string, handlers ...IHanlder) *Router {
	return newRouter(http.MethodDelete, path, handlers...)
}
