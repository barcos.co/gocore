package http

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/helpers"
	"gitlab.com/barcos.co/gocore/services"
	"gitlab.com/barcos.co/gocore/system"
)

type Server struct {
	routers      routersArray
	port         int
	ginEngine    *gin.Engine
	systemConfig system.SystemConfig
	apiRoot      string
}

func NewServer() *Server {
	s := Server{ginEngine: gin.Default()}
	port, err := strconv.Atoi(system.GetSystemConfigs().GetPort())
	if err != nil {
		log.Fatalf(fmt.Sprintf("%s couldnt be parsed", constants.HTTP_PORT_ENV))
	}

	s.port = port
	// TODO https://pkg.go.dev/github.com/gin-gonic/gin#readme-don-t-trust-all-proxies
	s.routers = routersArray{}
	return &s
}

func (s *Server) SetApiRoot(apiRoot string) *Server {
	s.apiRoot = apiRoot
	return s
}

func (s *Server) GetApiRoot() string {
	return s.apiRoot
}

func (s *Server) GetPort() int {
	return s.port
}

func (s *Server) SetSystemConfig(systemConfig system.SystemConfig) *Server {
	s.systemConfig = systemConfig
	return s
}

func (s *Server) ServeStatic(urlPath string, location string) {
	if s != nil && s.ginEngine != nil {
		s.ginEngine.Static(s.GetApiRoot()+urlPath, location)
	}
}

func (s *Server) Start() {

	helpers.RegisterCustomsValidators()

	corsConfig := cors.DefaultConfig()

	if system.GetSystemConfigs().IsDebugMode() {
		corsConfig.AllowAllOrigins = true
	} else {
		frontend := helpers.NewAppURL().FrontendURL
		corsConfig.AllowOrigins = []string{frontend}
		corsConfig.AllowAllOrigins = false
	}

	corsConfig.AllowHeaders = append(
		corsConfig.AllowHeaders,
		constants.AuthorizationHeader,
		constants.APIKeyHeader,
		constants.ContentLengthHeader,
		constants.ContentTypeHeader,
		constants.OriginHeader,
		constants.AuthorizationAccessTokenHeader,
		constants.AuthorizationRefresTokenHeader,
	)

	s.ginEngine.Use(cors.New(corsConfig))

	for _, router := range s.routers {

		switch router.method {
		case http.MethodGet:
			s.ginEngine.GET(router.path, router.handlers...)
		case http.MethodPut:
			s.ginEngine.PUT(router.path, router.handlers...)
		case http.MethodPost:
			s.ginEngine.POST(router.path, router.handlers...)
		case http.MethodDelete:
			s.ginEngine.DELETE(router.path, router.handlers...)
		default:
			log.Fatal(fmt.Printf("Method %s not handled\n", router.method))
		}
	}

	if err := s.ginEngine.Run(fmt.Sprintf(":%d", s.port)); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Http server successfully started at port %d\n", s.port)
}

func (s *Server) AddRouter(r *Router) *Server {

	uri := fmt.Sprintf("%s%s", s.GetApiRoot(), r.path)

	var handlers []gin.HandlerFunc

	if r.AuthRequired {
		handlers = append(handlers, services.AuthenticateRequest)
	}

	// if r.APIKeyRequired {
	// 	// TODO
	// 	// reqMiddlewares = append(reqMiddlewares, middlewares.ValidateAPIKey(server.systemConfig.GetKnownAPIs()))
	// }

	handlers = append(handlers, r.handlers...)

	r.setHandlers(handlers...)
	r.setPath(uri)
	s.routers = append(s.routers, r)
	return s
}
