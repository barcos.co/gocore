package system

import (
	"fmt"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/constants"
	"gitlab.com/barcos.co/gocore/logger"
)

var systemConfigs *SystemConfig

type SystemConfig struct {
	appName      string
	appMode      string
	serverPort   string
	domain       string
	frontendURL  string
	knownAPIKeys []string
}

func GetSystemConfigs() *SystemConfig {
	if systemConfigs == nil {
		readSystemConfigs()
		systemConfigs.display()
	}
	return systemConfigs
}

func (s *SystemConfig) display() {
	prefix := "System Config Read"
	logger.FLogInfof(gin.DefaultWriter, prefix, "AppName          \t\t --> %s \n", s.GetAppName())
	logger.FLogInfof(gin.DefaultWriter, prefix, "AppMode          \t\t --> %s \n", s.appMode)
	logger.FLogInfof(gin.DefaultWriter, prefix, "ServerPort       \t\t --> %s \n", s.GetPort())
	logger.FLogInfof(gin.DefaultWriter, prefix, "Domain           \t\t --> %s \n", s.GetDomain())
	logger.FLogInfof(gin.DefaultWriter, prefix, "FrontendURL   \t\t --> %s \n", s.GetFrontendURL())

	// logger.FLogInfof(gin.DefaultWriter, prefix, "ServiceURL   \t\t --> %s \n\n", s.GetServiceURL())
}

func readSystemConfigs() {
	if systemConfigs == nil {
		newSystemConfigs()
	}
	systemConfigs.setPort(readPort())
	systemConfigs.setDomain(readDomain())
	systemConfigs.setFrontendURL(readFrontendURL())
	systemConfigs.SetAppName(readAppName())
	systemConfigs.setAppMode(readAppMode())
}

func newSystemConfigs() *SystemConfig {
	systemConfigs = &SystemConfig{}
	return systemConfigs
}

func (s *SystemConfig) AddKnownAPI(api string) *SystemConfig {
	s.knownAPIKeys = append(s.knownAPIKeys, api)
	return s
}

func (s *SystemConfig) GetKnownAPIs() []string {
	return s.knownAPIKeys
}

func readPort() int {
	return readHttpPort()
}

func readHttpPort() int {
	port, err := strconv.Atoi(os.Getenv(constants.HTTP_PORT_ENV))
	if err != nil {
		port = 3000
	}
	return port
}

func (s *SystemConfig) setPort(port int) *SystemConfig {
	s.serverPort = fmt.Sprintf("%d", port)
	return s
}

func (s *SystemConfig) GetPort() string {
	return s.serverPort
}

func readDomain() string {
	domain := os.Getenv(constants.DOMAIN_ENV)
	if domain == "" {
		domain = "localhost"
	}
	return domain
}

func (s *SystemConfig) setDomain(domain string) *SystemConfig {
	s.domain = domain
	return s
}

func (s *SystemConfig) GetDomain() string {
	return s.domain
}

func readFrontendURL() string {
	url := os.Getenv(constants.FRONTEND_URL_ENV)
	if url == "" {
		url = "localhost"
	}
	return url
}

func (s *SystemConfig) setFrontendURL(url string) *SystemConfig {
	s.frontendURL = url
	return s
}

func (s *SystemConfig) GetFrontendURL() string {
	return s.frontendURL
}

func readAppName() string {
	return os.Getenv(constants.APP_NAME_ENV)
}

func (s *SystemConfig) SetAppName(appName string) *SystemConfig {
	s.appName = appName
	return s
}

func (s *SystemConfig) GetAppName() string {
	if s.appName != "" {
		return s.appName
	}
	return "Unkown App"
}

func readAppMode() string {
	return os.Getenv(constants.APP_MODE_ENV)
}

func (s *SystemConfig) setAppMode(mode string) {
	if mode == gin.ReleaseMode {
		s.SetReleaseMode()
	} else {
		s.SetDebugMode()
	}
}

func (s *SystemConfig) SetDebugMode() *SystemConfig {
	gin.SetMode(gin.DebugMode)
	s.appMode = gin.DebugMode
	return s
}

func (s *SystemConfig) SetReleaseMode() *SystemConfig {
	gin.SetMode(gin.ReleaseMode)
	s.appMode = gin.ReleaseMode
	return s
}

func (s *SystemConfig) IsDebugMode() bool {
	return gin.IsDebugging()
}

func (s *SystemConfig) IsReleaseMode() bool {
	return gin.Mode() == gin.ReleaseMode
}
