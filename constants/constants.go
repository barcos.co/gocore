package constants

import "time"

const (
	HealthPath = "/health"
	UsersPath  = "/users"
)

const CoreTimeFormat = time.RFC1123

const (
	Inactive  = "inactive"
	Closed    = "closed"
	Created   = "created"
	Accepted  = "accepted"
	Rejected  = "rejected"
	Paid      = "paid"
	Confirmed = "confirmed"
	Cancelled = "cancelled"
	Succeeded = "succeeded"
)

const (
	IDParam                = "id"
	EmailParam             = "email"
	VerificationTokenParam = "verification_token"
	OTPTokenParam          = "otp_token"
)

const (
	ReqJWTKey    = "JWTKey"
	ReqClaimsKey = "ClaimsKey"
	ReqAPIKey    = "APIKey"
)

const (
	AuthorizationHeader            = "Authorization"
	AuthorizationAccessTokenHeader = "Authorization-Access-Token"
	AuthorizationRefresTokenHeader = "Authorization-Refresh-Token"
	AcceptLanguageHeader           = "Accept-Language"
	OriginHeader                   = "Origin"
	ContentLengthHeader            = "Content-Length"
	ContentTypeHeader              = "Content-Type"
	APIKeyHeader                   = "X-API-Key"

	BearerTokenType = "Bearer"

	CacheControlHeader = "Cache-Control"
	PragmaHeader       = "Pragma"
	NoStore            = "no-store"
	NoCache            = "no-cache"
)

const (
	RefreshTokenGrantType = "refresh_token"
	PasswordGrantType     = "password"
)

const (
	ApplicationJSONContentType = "application/json; charset=utf-8"
	TextPlainContentType       = "text/plain; charset=utf-8"
	TextHTMLContentType        = "text/html; charset=utf-8"

	// ... add more
)

const (
	DOMAIN_ENV    = "DOMAIN"
	HTTP_PORT_ENV = "HTTP_PORT"

	APP_NAME_ENV        = "APP_NAME"
	APP_MODE_ENV        = "APP_MODE"
	AUTH_JWT_SECRET_ENV = "AUTH_JWT_SECRET"

	FRONTEND_URL_ENV = "FRONTEND_URL"
)
