package constants

// Gender represents any of existing/supported genders for persons
type UserGender string

const (
	male = "male"
	Male = UserGender(male)

	female = "female"
	Female = UserGender(female)

	other = "other"
	Other = UserGender(other)
)

// UserState represents any of existing/supported states for a user persons
type UserState string

const (
	pending = "pending"
	Pending = UserState(pending)

	active = "active"
	Active = UserState(active)

	password_reset = "password_reset"
	PasswordReset  = UserState(password_reset)

	locked = "locked"
	Locked = UserState(locked)
)
