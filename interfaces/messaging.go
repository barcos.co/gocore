package interfaces

import "gitlab.com/barcos.co/gocore/errors"

type IMessagingServiceTemplate interface {
	GetTemplateID() int
	GetSubject() string
	GetSenderEmail() string
	GetSenderName() string
	GetReciverEmail() string
	GetReciverName() string
	GetVariables() map[string]interface{}
}

type IMessagingService interface {
	SendEmail(email string) (bool, errors.Error)
	SendSMS(number string) (bool, errors.Error)
	SendIOSPushNotification(deviceId string) (bool, errors.Error)
	SendEmailWithTemplate(template IMessagingServiceTemplate) (bool, errors.Error)
	IBaseService
}
