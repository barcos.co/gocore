package interfaces

import (
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"
)

type IOTPAuthController interface {
	// Repositories
	HasOTPAuthRepository() bool
	GetOTPAuthRepository() IOTPAuthRepository

	// Services
	HasOTPAuthService() bool
	GetOTPAuthService() IOTPAuthService
	HasOTPAuthMessagingService() bool
	GetOTPAuthMessagingService() IOTPAuthMessagingService

	IBaseAuthController
}

type IOTPAuthRepository interface {
	IOTPAuthControllerRef
	IBaseAuthRepository
}

type IOTPAuthService interface {
	Login(email string) errors.Error
	RegisterUserByEmail(payload request.OTPAuthRegisterPayload) (*models.User, errors.Error)
	AuthenticateUser(payload request.OTPAuthPayload) (*request.AuthToken, errors.Error)

	IOTPAuthControllerRef
	IBaseAuthService
}

type IOTPAuthMessagingService interface {
	SendOTPLoginEmail(payload request.OTPAuthLoginMessagingPayload) (bool, errors.Error)

	IOTPAuthControllerRef
	IMessagingService
}

type IOTPAuthControllerRef interface {
	GetOTPAuthController() IOTPAuthController
	SetOTPAuthController(controller IOTPAuthController)
}
