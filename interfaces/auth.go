package interfaces

import (
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/request"
)

type IAuthController interface {
	// Repositories
	HasAuthRepository() bool
	GetAuthRepository() IAuthRepository

	// Services
	HasAuthService() bool
	GetAuthService() IAuthService
	HasAuthMessagingService() bool
	GetAuthMessagingService() IAuthMessagingService

	// Paths
	SetVerifyAccountPath(verifyAccountPath string)
	GetVerifyAccountPath() string
	SetResetPasswordPath(resetPasswordPath string)
	GetResetPasswordPath() string

	IBaseAuthController
}

type IAuthRepository interface {
	IAuthControllerRef
	IBaseAuthRepository
}

type IAuthService interface {
	RegisterUserByEmail(payload request.AuthRegisterPayload) (*models.User, errors.Error)
	VerifyUserEmail(payload request.AuthVerficationPayload) errors.Error

	AuthenticateUser(email string, password string) (*request.AuthToken, errors.Error)
	RefreshAccessToken(refreshToken string) (*request.AuthToken, errors.Error)
	// TODO -> the user can reset a pwd by clicking a btn. He/she recive an email with a comfirmation link
	// on that link he could create another password
	SendResetPasswordEmail(email string) errors.Error                 // -> send email for restet
	ResetPassword(payload request.AuthResetPasswordJSON) errors.Error // verify token and so change pwd

	IAuthControllerRef
	IBaseAuthService
}

type IAuthMessagingService interface {
	SendVerifyEmail(payload request.AuthRegisterMessagingPayload) (bool, errors.Error)
	SendResetPasswordEmail(payload request.AuthResetPasswordEmailMessagingPayload) (bool, errors.Error)

	IAuthControllerRef
	IMessagingService
}

type IAuthControllerRef interface {
	GetAuthController() IAuthController
	SetAuthController(controller IAuthController)
}
