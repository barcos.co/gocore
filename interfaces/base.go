package interfaces

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/models"
)

type IHanlder = gin.HandlerFunc

type IBaseController interface {
	IAbstractController
}

type IBaseAuthController interface {
	SetRegistrationPath(registrationPath string)
	GetRegistrationPath() string

	SetTokenPath(tokenPath string)
	GetTokenPath() string

	SetProfilePath(profilePath string)
	GetProfilePath() string

	SetValidationTokenPath(validationPath string)
	GetValidationTokenPath() string
	IBaseController
}

type IBaseService interface {
	IAbstractService
}

type IBaseAuthService interface {
	ValidateToken(ctx *gin.Context) // verify token and so change pwd
	FindProfileByEmail(email string) (*models.User, errors.Error)
	IBaseService
}

type IBaseRepository interface {
	IAbstractRepository
}

type IBaseAuthRepository interface {
	FindAuthUserByEmail(email string) (*models.User, errors.Error)
	UpdateAuthUserByEmail(email string, data UpdateDocument) (bool, errors.Error)
	VerifyAuthUserByEmail(email string) (bool, errors.Error)
	DeleteAuthUserByEmail(email string) (bool, errors.Error)

	AddAuthUser(user *models.User) (string, errors.Error)
	IBaseRepository
}
