package interfaces

import "github.com/gin-gonic/gin"

type IGetController interface {
	Find(ctx *gin.Context)
}
type IPostController interface {
	Add(ctx *gin.Context)
}

type IPutController interface {
	Update(ctx *gin.Context)
}

type IDeleteController interface {
	Delete(ctx *gin.Context)
}
