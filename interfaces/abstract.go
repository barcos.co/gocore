package interfaces

import (
	"gitlab.com/barcos.co/gocore/errors"
)

type INamedInstance interface {
	GetName() string
	SetName(name string)
}

type IAbstractController interface {
	SetApiRoot(apiRoot string)
	GetApiRoot() string
	INamedInstance
}

type IAbstractService interface {
	INamedInstance
}

type DocumentModel = interface{}
type MapDocument = map[string]DocumentModel

type AddDocument = DocumentModel
type UpdateDocument = MapDocument
type QueryDocument = MapDocument

type IAbstractRepository interface {
	Connect() (bool, errors.Error)

	FindOneDocumentByID(Id string, documentModel DocumentModel, collection string) (DocumentModel, errors.Error)
	FindOneDocumentByQuery(query QueryDocument, documentModel DocumentModel, collection string) (DocumentModel, errors.Error)
	FindManyDocumentsByID(documentIDs []string, documentModel DocumentModel, collection string) ([]DocumentModel, errors.Error)

	AddDocument(document AddDocument, collection string) (string, errors.Error)

	UpdateOneDocumentByID(Id string, document UpdateDocument, collection string) (bool, errors.Error)
	UpdateOneDocumentByQuery(query QueryDocument, document UpdateDocument, collection string) (bool, errors.Error)

	DeleteOneDocumentByID(Id string, collection string) (bool, errors.Error)
	DeleteOneDocumentByQuery(query QueryDocument, collection string) (bool, errors.Error)
	INamedInstance
}
